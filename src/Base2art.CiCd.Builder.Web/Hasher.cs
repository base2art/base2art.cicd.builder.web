namespace Base2art.CiCd.Builder.Web
{
    using System;
    using System.Globalization;
    using System.Security.Cryptography;
    using System.Text;

    internal static class Hasher
    {
        public static Guid HashAsGuid(this string value)
        {
            return Create(value, x => new Md5HashResult(x), MD5.Create).AsGuid();
        }

        private static TResult Create<T, TResult>(string value, Func<byte[], TResult> creator, Func<T> provider)
            where T : HashAlgorithm
            where TResult : HashResult
        {
            var inputBytes = DefaultEncoding().GetBytes(value ?? string.Empty);
            using (var hashAlgorithm = provider())
            {
                return creator(hashAlgorithm.ComputeHash(inputBytes));
            }
        }

        private static Encoding DefaultEncoding() => Encoding.UTF8;

        private class HashResult
        {
            protected HashResult(byte[] hashedBytes) => this.HashedBytes = hashedBytes;

            protected byte[] HashedBytes { get; }

            public byte[] AsByteArray() => this.HashedBytes;

            public string AsString()
            {
                var hash = this.HashedBytes;
                // step 2, convert byte array to hex string
                var sb = new StringBuilder();
                for (var i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("X2", CultureInfo.InvariantCulture));
                }

                return sb.ToString();
            }
        }

        private class Md5HashResult : HashResult
        {
            public Md5HashResult(byte[] hashedBytes) : base(hashedBytes)
            {
            }

            public Guid AsGuid()
            {
                var hashedBytes = this.HashedBytes;
                var bytes = new byte[16];
                bytes[0] = hashedBytes[3];
                bytes[1] = hashedBytes[2];
                bytes[2] = hashedBytes[1];
                bytes[3] = hashedBytes[0];
                bytes[4] = hashedBytes[5];
                bytes[5] = hashedBytes[4];
                bytes[6] = hashedBytes[7];
                bytes[7] = hashedBytes[6];
                bytes[8] = hashedBytes[8];
                bytes[9] = hashedBytes[9];
                bytes[10] = hashedBytes[10];
                bytes[11] = hashedBytes[11];
                bytes[12] = hashedBytes[12];
                bytes[13] = hashedBytes[13];
                bytes[14] = hashedBytes[14];
                bytes[15] = hashedBytes[15];
                return new Guid(bytes);
            }
        }
    }
}