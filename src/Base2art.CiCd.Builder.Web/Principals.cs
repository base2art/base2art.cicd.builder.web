namespace Base2art.CiCd.Builder.Web
{
    using System.Security.Principal;

    public static class Principals
    {
        public static void RequireSignedIn(this IPrincipal principal)
        {
            var isAuthenticated = principal?.Identity?.IsAuthenticated;

            if (!isAuthenticated.GetValueOrDefault())
            {
                throw new Base2art.Web.Exceptions.NotAuthenticatedException();
            }
        }

        public static void RequireSignedIn(this IRequireAuthProvider auth, IPrincipal principal)
        {
            if (auth.Value)
            {
                principal.RequireSignedIn();
            }
        }
    }
}