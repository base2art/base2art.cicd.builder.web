namespace Base2art.CiCd.Builder.Web
{
    public class RequireAuthProvider : IRequireAuthProvider
    {
        private readonly bool value;
        
        private static bool? overridingValue;

        public static bool OverridingValue
        {
            get => overridingValue.GetValueOrDefault(true);
            set => overridingValue = value;
        }

        public bool Value => overridingValue.HasValue ? overridingValue.Value : this.value;

        public RequireAuthProvider(bool value) => this.value = value;
    }
}