namespace Base2art.CiCd.Builder.Web
{
    using System.Collections.Generic;
    using System.Data.Common;
    using System.IO;
    using System.Linq;
    using DataStorage;

    public class DbmsFactory : Base2art.DataStorage.DbmsFactory
    {
        public DbmsFactory(IDirectoryProvider directoryProvider, List<TypedNamedConnectionString> items, IDataStorageProvider[] providers)
            : base(items.Select(x => Convert(x, directoryProvider)).ToList(), providers)
        {
        }

        private static TypedNamedConnectionString Convert(
            TypedNamedConnectionString typedNamedConnectionString,
            IDirectoryProvider directoryProvider)
        {
            var builder = new DbConnectionStringBuilder(true);
            builder.ConnectionString = typedNamedConnectionString.ConnectionString;

            if (builder.ContainsKey("Data Source"))
            {
                builder["Data Source"] = Path.Combine(directoryProvider.DataDirectory().FullName,
                                                      builder["Data Source"]?.ToString() ?? "builds.sqlite");
            }

            return new TypedNamedConnectionString
                   {
                       AdditionalParameters = typedNamedConnectionString.AdditionalParameters,
                       Name = typedNamedConnectionString.Name,
                       ConnectionString = builder.ConnectionString,
                       ProviderClassName = typedNamedConnectionString.ProviderClassName,
                   };
        }
    }
}