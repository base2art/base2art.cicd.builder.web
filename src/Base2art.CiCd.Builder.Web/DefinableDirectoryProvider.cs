namespace Base2art.CiCd.Builder.Web
{
    using System;
    using System.IO;

    public class DefinableDirectoryProvider : IDirectoryProvider
    {
        private readonly DirectoryInfo info;
        private DirectoryInfo data;

        public DefinableDirectoryProvider(string path)
        {
            if (string.IsNullOrWhiteSpace(path) || path.StartsWith("#"))
            {
                path = Path.Combine(
                                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                    "base2art/cicd/build-node");
            }

            this.info = Directory.CreateDirectory(Path.Combine(path, "wd"));
            this.data = Directory.CreateDirectory(Path.Combine(path, "data"));
        }

        public DirectoryInfo GetWorkingDir() => this.info;
        public DirectoryInfo DataDirectory() => this.data;
    }
}