namespace Base2art.CiCd.Builder.Web
{
    public interface IRequireAuthProvider
    {
        bool Value { get; }
    }
}