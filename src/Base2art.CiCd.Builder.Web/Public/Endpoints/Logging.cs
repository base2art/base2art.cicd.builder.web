﻿namespace Base2art.CiCd.Builder.Web.Public.Endpoints
{
    using System;
    using System.IO;
    using System.Threading.Tasks;

    public static class Logging
    {
        public static async Task Log(Exception exception)
        {
            using (var sr = CreateFile())
            {
                await sr.WriteLineAsync(exception.GetType().FullName);
                await sr.WriteLineAsync(exception.Message);
                await sr.WriteLineAsync(exception.StackTrace);
                await sr.FlushAsync();
            }
        }

        public static TextWriter CreateFile()
        {
            var dir = Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "logs"));

            var combine = Path.Combine(dir.FullName, $"{DateTime.UtcNow.Ticks:X}.app_log");
            var writer = new FileStream(combine, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite);
            return new StreamWriter(writer);
        }
    }
}