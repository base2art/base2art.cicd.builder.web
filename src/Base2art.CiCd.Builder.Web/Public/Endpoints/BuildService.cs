﻿namespace Base2art.CiCd.Builder.Web.Public.Endpoints
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Data.dbo;
    using DataStorage;
    using Diagnostic;
    using FeatureDiscovery;
    using IO;
    using Base2art.Threading.Tasks;
    using Features;
    using Resources;
    using Servers;

    public class BuildService : IBuildPhaseService
    {
//        private readonly DirectoryInfo baseDir;
        private readonly bool deleteOnCompletion;

        private readonly Guid machineId;
        private readonly ProcessBuilderFactory processBuilder;
        private readonly IFeatureDiscoveryProvider[] providers;
        private readonly IDirectoryProvider directoryProvider;
        private readonly IRequireAuthProvider auth;
        private readonly IDataStore store;

        public BuildService(
            IDataStoreFactory store,
            IDirectoryProvider directoryProvider,
            IRequireAuthProvider auth,
            bool deleteOnCompletion,
            IFeatureDiscoveryProvider[] providers)
        {
            this.machineId = Environment.MachineName.HashAsGuid();
            this.directoryProvider = directoryProvider;
            this.auth = auth;
            this.deleteOnCompletion = deleteOnCompletion;
            this.providers = providers;

            this.processBuilder = new ProcessBuilderFactory();
            this.store = store.Create("builds");
        }

        public async Task Remove(Guid buildId, ClaimsPrincipal principal)
        {
            this.auth.RequireSignedIn(principal);

            var buildDir = Directory.CreateDirectory(Path.Combine(this.directoryProvider.GetWorkingDir().FullName, this.ForDir(buildId)));

            var mapItems = await this.store.Select<node_build_source_v2>()
                                     .Where(rs => rs.Field(x => x.build_id, buildId.ToString("N"), (x, y) => x == y))
                                     .Execute();

            foreach (var mapItem in mapItems)
            {
                var dir = new DirectoryInfo(Path.Combine(buildDir.FullName, mapItem.short_id));

                if (dir.Exists)
                {
                    TaskTrapper.TryRunSafe<IOException>(3, () =>
                    {
                        if (dir.Exists)
                        {
                            dir.Delete(true);
                        }
                    });
                }
            }
        }

        public async Task<SupportedFeature[]> GetSupportedFeatures(ClaimsPrincipal principal)
        {
            this.auth.RequireSignedIn(principal);
            var localProviders = this.providers.Concat(new[] {new MachineDomainFeatureProvider()});
            var items = await Task.WhenAll(localProviders.Select(x => x.CreateFeatures()));
            var itemsPair = await Task.WhenAll(items.SelectMany(x => x)
                                                    .Concat(new[] {new MachineNameFeature()})
                                                    .Select(async x => Tuple.Create(x.FeatureName, x.GetType(), await x.HasFeature())));
            return itemsPair.Select(x => x)
                            .Where(x => x.Item3)
                            .Select(x => new SupportedFeature
                                         {
                                             Id = x.Item1.HashAsGuid(),
                                             Name = x.Item1,
                                             FullName = x.Item2.FullName,
                                             DefaultData = null
                                         })
                            .ToArray();
        }

        public async Task<Dictionary<Guid, Artifact[]>> GetArtifacts(Guid buildId, ClaimsPrincipal principal)
        {
            this.auth.RequireSignedIn(principal);

            var buildDir =
                Directory.CreateDirectory(Path.Combine(this.directoryProvider.GetWorkingDir().FullName, this.ForDir(buildId), ".artifacts"));

            var mapItems = await this.store.Select<node_build_source_v2>()
                                     .Where(rs => rs.Field(x => x.build_id, buildId.ToString("N"), (x, y) => x == y))
                                     .Execute();

            var artifactses = new Dictionary<Guid, Artifact[]>();

            foreach (var mapItem in mapItems)
            {
                var dir = new DirectoryInfo(Path.Combine(buildDir.FullName, mapItem.short_id));

                if (dir.Exists)
                {
                    var items = dir.FindAllFiles()
                                   .Select(x => new Artifact
                                                {
                                                    Name = x.FileEntry.Name,
                                                    RelativeName = x.RelativeName,
                                                    Content = File.ReadAllBytes(x.FileEntry.FullName)
                                                })
                                   .ToArray();

                    artifactses[new Guid(mapItem.long_id)] = items;
                }
            }

            return artifactses;
        }

        public Task<ProjectBuild> GetBuild(Guid buildId, ClaimsPrincipal principal)
        {
            Console.WriteLine(buildId);
            this.auth.RequireSignedIn(principal);

            return this.GetBuildInternal(buildId);
        }

        public Task<ProjectBuild> StartBuild(Guid buildId, string version, List<ProjectBuildContent> buildData, ClaimsPrincipal principal)
        {
            this.auth.RequireSignedIn(principal);

            if (buildId == Guid.Empty)
            {
                buildId = Guid.NewGuid();
            }

            this.Build(buildId, buildData, version).RunAway(async x =>
            {
                await Logging.Log(x);

                await this.store.Update<node_build_v2>()
                          .Where(rs => rs.Field(s => s.id, buildId.ToString("N"), (z, y) => z == y))
                          .Set(rs => rs.Field(s => s.completed_at, DateTime.UtcNow)
                                       .Field(s => s.state, (int) BuildPhaseState.CompletedFail))
                          .Execute();
            });

            return Task.FromResult(new ProjectBuild
                                   {
                                       CompletedAt = null,
                                       State = BuildPhaseState.Pending,
                                       BuildId = buildId,
                                       Error = "",
                                       Output = ""
                                   });
        }

        private async Task<ProjectBuild> GetBuildInternal(Guid buildId)
        {
            return await this.store.SelectSingle<node_build_v2>()
                             .Where(rs => rs.Field(x => x.id, buildId.ToString("N"), (x, y) => x == y))
                             .Execute()
                             .Then()
                             .Return(x => new ProjectBuild
                                          {
                                              BuildId = buildId,
                                              CompletedAt = x.completed_at,
                                              Output = this.GetOutputFile(buildId),
                                              Error = this.GetErrorFile(buildId),
                                              State = (BuildPhaseState) x.state
                                          });
        }

        private async Task<ProjectBuild> Build(Guid buildId, List<ProjectBuildContent> buildData, string version)
        {
            var sourceControlData = buildData;

            var buildDir = Directory.CreateDirectory(Path.Combine(this.directoryProvider.GetWorkingDir().FullName, this.ForDir(buildId)));

            await this.store.Insert<node_build_v2>()
                      .Record(rs => rs.Field(s => s.id, buildId.ToString("N"))
                                      .Field(s => s.machine_id, this.machineId)
                                      .Field(s => s.state, (int) BuildPhaseState.Pending))
                      .Execute();

            var inserter = this.store.Insert<node_build_source_v2>();

            foreach (var buildDatum in buildData)
            {
                inserter.Record(rs => rs.Field(s => s.build_id, buildId.ToString("N"))
                                        .Field(s => s.short_id, this.ForDir(buildDatum.SourceControlId))
                                        .Field(s => s.long_id, buildDatum.SourceControlId.ToString("N")));
            }

            await inserter.Execute();

            await Task.WhenAll(sourceControlData.Select(projectBuildContent => this.ExtractAll(
                                                                                               projectBuildContent.SourceControlId,
                                                                                               buildDir,
                                                                                               projectBuildContent.Files,
                                                                                               projectBuildContent.BranchName,
                                                                                               projectBuildContent.BranchHash,
                                                                                               version)));

            foreach (var projectBuildContent in sourceControlData)
            {
                for (var index = 0; index < projectBuildContent.BuildInstructions.Length; index++)
                {
                    var sourceControlId = projectBuildContent.SourceControlId;
                    var errFile = Path.Combine(buildDir.FullName, $"{this.ForDir(sourceControlId)}${index:0000}.err");
                    var outFile = Path.Combine(buildDir.FullName, $"{this.ForDir(sourceControlId)}${index:0000}.out");
                    var instruction = projectBuildContent.BuildInstructions[index];
                    using (var errStream = new FileStream(errFile, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite))
                    {
                        using (var outStream = new FileStream(outFile, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite))
                        {
                            using (var outWriter = new StreamWriter(outStream))
                            {
                                using (var errWriter = new StreamWriter(errStream))
                                {
                                    await this.store.Update<node_build_v2>()
                                              .Where(rs => rs.Field(s => s.id, buildId.ToString("N"), (x, y) => x == y))
                                              .Set(rs => rs.Field(s => s.completed_at, DateTime.UtcNow)
                                                           .Field(s => s.state, (int) BuildPhaseState.Working))
                                              .Execute();

                                    int result;

                                    try
                                    {
                                        var exeFileInfo = await Environments.FindFileInPath(instruction.Executable);
                                        var exe = exeFileInfo == null ? instruction.Executable : exeFileInfo.FullName;

                                        result = await this.processBuilder.Create()
                                                           .WithErrorWriter(errWriter)
                                                           .WithOutputWriter(outWriter)
                                                           .WithArguments(instruction.Arguments)
                                                           .WithExecutable(exe)
                                                           .WithUser(instruction.Username)
                                                           .WithPassword(instruction.Password)
                                                           .WithWorkingDirectory(Path.Combine(buildDir.FullName, this.ForDir(sourceControlId)))
                                                           .Execute();
                                    }
                                    catch (Exception e)
                                    {
                                        result = -1;

                                        this.DebugWriter(errWriter, e);
                                    }

                                    if (result != 0)
                                    {
                                        await this.store.Update<node_build_v2>()
                                                  .Where(rs => rs.Field(s => s.id, buildId.ToString("N"), (x, y) => x == y))
                                                  .Set(rs => rs.Field(s => s.completed_at, DateTime.UtcNow)
                                                               .Field(s => s.state, (int) BuildPhaseState.CompletedFail))
                                                  .Execute();

                                        return await this.GetBuildInternal(buildId);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            var artifactsDir = Directory.CreateDirectory(Path.Combine(buildDir.FullName, ".artifacts"));

            foreach (var datum in buildData)
            {
                var dir = new DirectoryInfo(Path.Combine(buildDir.FullName, this.ForDir(datum.SourceControlId)));

                var path = Path.Combine(dir.FullName, ".artifacts");
                var content = !File.Exists(path) ? new string[] { } : File.ReadAllLines(path);

                var artifactDir = Directory.CreateDirectory(Path.Combine(artifactsDir.FullName, dir.Name));

                foreach (var file in dir.FindMatchingFiles(content, MatchingMode.Contained))
                {
                    Console.WriteLine(file.FileEntry.FullName);
                    Directory.CreateDirectory(Path.Combine(artifactDir.FullName, Path.GetDirectoryName(file.RelativeName)));

                    file.FileEntry.CopyTo(Path.Combine(artifactDir.FullName, file.RelativeName));
                }

                if (this.deleteOnCompletion)
                {
                    try
                    {
                        dir.Delete(true);
                    }
                    catch (IOException)
                    {
                    }
                }
            }

            await this.store.Update<node_build_v2>()
                      .Where(rs => rs.Field(s => s.id, buildId.ToString("N"), (x, y) => x == y))
                      .Set(rs => rs.Field(s => s.completed_at, DateTime.UtcNow)
                                   .Field(s => s.state, (int) BuildPhaseState.CompletedSuccess))
                      .Execute();

            return await this.GetBuildInternal(buildId);
        }

        private void DebugWriter(StreamWriter errWriter, Exception exception)
        {
            if (exception == null)
            {
                return;
            }

            errWriter.WriteLine(exception.GetType().FullName);
            errWriter.WriteLine(exception.Message);
            errWriter.WriteLine(exception.StackTrace);

            this.DebugWriter(errWriter, exception.InnerException);
        }

        private string ForDir(Guid g) => g.ToString("N").Substring(12);

        private string GetOutputFile(Guid buildId)
        {
            var buildDir = Directory.CreateDirectory(Path.Combine(this.directoryProvider.GetWorkingDir().FullName, this.ForDir(buildId)));
            var outs = buildDir.GetFiles("*.out");
            var nl = Environment.NewLine;
            return string.Join(
                               $"{nl}{nl}--------------------------------{nl}{nl}",
                               outs.Select(this.GetFile));
        }

        private string GetErrorFile(Guid buildId)
        {
            var buildDir = Directory.CreateDirectory(Path.Combine(this.directoryProvider.GetWorkingDir().FullName, this.ForDir(buildId)));
            var outs = buildDir.GetFiles("*.err");
            var nl = Environment.NewLine;
            return string.Join(
                               $"{nl}{nl}--------------------------------{nl}{nl}",
                               outs.Select(this.GetFile));
        }

        private string GetFile(FileInfo file)
        {
            if (file.Exists)
            {
                using (var stream = File.Open(file.FullName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    using (var reader = new StreamReader(stream))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }

            return file.FullName;
        }

        private Task ExtractAll(
            Guid sourceControlId,
            DirectoryInfo dir,
            byte[] contentPairValue,
            string branchName,
            string branchHash,
            string version)
        {
            return Task.Factory.StartNew(() => this.ExtractAllSync(sourceControlId, dir, contentPairValue, branchName, branchHash, version));
        }

        private void ExtractAllSync(
            Guid sourceControlId,
            DirectoryInfo dir,
            byte[] contentPairValue,
            string branchName,
            string branchHash,
            string version)
        {
            var fullName = Path.Combine(dir.FullName, this.ForDir(sourceControlId));

            ExtractZip(fullName, contentPairValue);

            var namePath = Path.Combine(fullName, ".branch-name");
            var hashPath = Path.Combine(fullName, ".branch-hash");
            var versionPath = Path.Combine(fullName, ".version");

            File.WriteAllText(namePath, branchName);
            File.WriteAllText(hashPath, branchHash);
            if (!string.IsNullOrWhiteSpace(version))
            {
                File.WriteAllText(versionPath, version);
            }
        }

        private static void ExtractZip(string fullName, byte[] contentPairValue)
        {
            using (var ms = new MemoryStream())
            {
                ms.Write(contentPairValue, 0, contentPairValue.Length);

                ms.Seek(0L, SeekOrigin.Begin);
                using (var source = new ZipArchive(ms))
                {
                    foreach (var entry in source.Entries)
                    {
                        var fullPath = Path.GetFullPath(Path.Combine(fullName, entry.FullName));
                        if (!fullPath.StartsWith(fullName, StringComparison.OrdinalIgnoreCase))
                        {
                            throw new IOException("IO_ExtractingResultsInOutside");
                        }

                        if (Path.GetFileName(fullPath).Length == 0)
                        {
                            if (entry.Length != 0L)
                            {
                                throw new IOException("IO_DirectoryNameWithData");
                            }

                            Directory.CreateDirectory(fullPath);
                        }
                        else
                        {
                            Directory.CreateDirectory(Path.GetDirectoryName(fullPath));
                            entry.ExtractToFile(fullPath, false);
                        }
                    }
                }
            }
        }
    }
}

//        private Artifact CreateArtifact(KeyValuePair<string, FileInfo> item) => new Artifact
//                                                                                {
//                                                                                    Content = File.ReadAllBytes(item.Value.FullName),
//                                                                                    RelativeName = item.Key,
//                                                                                    Name = item.Value.Name
//                                                                                };