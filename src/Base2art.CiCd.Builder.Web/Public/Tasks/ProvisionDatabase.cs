namespace Base2art.CiCd.Builder.Web.Public.Tasks
{
    using Data.dbo;
    using DataStorage;
    using Servers;

    public class ProvisionDatabaseTask
    {
        private readonly IDbms dbms;
        private readonly IDataStore store;

        public ProvisionDatabaseTask(IDbmsFactory dbmsFactory, IDataStoreFactory store)
        {
            this.dbms = dbmsFactory.Create("builds");
            this.store = store.Create("builds");
        }

        public void ExecuteAsync()
        {
            this.dbms.CreateTable<node_build_v2>().Execute().RunAway();
            this.dbms.CreateTable<node_build_source_v2>().Execute().RunAway();
        }
    }
}