namespace Base2art.CiCd.Builder.Web.Features
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.NetworkInformation;
    using System.Threading.Tasks;
    using FeatureDiscovery;

    public class MachineDomainFeatureProvider : IFeatureDiscoveryProvider
    {
        public Task<IEnumerable<IFeatureDiscovery>> CreateFeatures()
        {
            var domainName = this.Trap(GetDomainName);

            List<IFeatureDiscovery> features = new List<IFeatureDiscovery>();

            if (!string.IsNullOrWhiteSpace(domainName))
            {
                features.Add(new EmbeddedFeature($"Domain: {domainName.ToUpperInvariant()}"));
            }

            var suffixes = this.Trap(DnsSuffixes, () => new string[0]);
            features.AddRange(suffixes.Select(x => new EmbeddedFeature($"Network: {x.ToUpperInvariant()}")));

            return Task.FromResult<IEnumerable<IFeatureDiscovery>>(features.ToArray());
        }

        private T Trap<T>(Func<T> trap, Func<T> defaultLookup = null)
        {
            try
            {
                return trap();
            }
            catch (Exception)
            {
                return defaultLookup == null ? default(T) : defaultLookup();
            }
        }

        private static string GetDomainName()
        {
            var properties = IPGlobalProperties.GetIPGlobalProperties();
            var domainName = properties.DomainName;

            var comp = StringComparison.OrdinalIgnoreCase;
            if (string.Equals(domainName, "none", comp) || string.Equals(domainName, "(none)", comp))
            {
                return null;
            }

            return domainName;
        }

        private static IEnumerable<string> DnsSuffixes()
        {
            return NetworkInterface.GetAllNetworkInterfaces()
                                   .Select(property => property.GetIPProperties().DnsSuffix)
                                   .Where(dnsSuffix => !string.IsNullOrWhiteSpace(dnsSuffix))
                                   .Distinct(StringComparer.OrdinalIgnoreCase);
        }

        private class EmbeddedFeature : IFeatureDiscovery
        {
            private readonly string name;

            public EmbeddedFeature(string name)
            {
                this.name = name;
            }

            public Task<bool> HasFeature() => Task.FromResult(true);

            public string FeatureName => this.name;
        }
    }
}