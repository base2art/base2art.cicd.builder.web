namespace Base2art.CiCd.Builder.Web.Features
{
    using System;
    using System.Threading.Tasks;
    using Base2art.FeatureDiscovery;

    public class MachineNameFeature : IFeatureDiscovery
    {
        public Task<bool> HasFeature()
        {
            return Task.FromResult(!string.Equals(Environment.MachineName, "localhost", StringComparison.OrdinalIgnoreCase));
        }

        public string FeatureName => $"Machine: {Environment.MachineName.ToUpperInvariant()}";
    }
}