﻿namespace Base2art.CiCd.Builder.Web.Data.dbo
{
    using System;

    public interface node_build_v2
    {
        string id { get; }

        int state { get; }

        Guid machine_id { get; }

        Guid? claimed_by { get; }

        DateTime? completed_at { get; }
    }
}