namespace Base2art.CiCd.Builder.Web.Data.dbo
{
    public interface node_build_source_v2
    {
        string build_id { get; }

        string short_id { get; }

        string long_id { get; }
    }
}