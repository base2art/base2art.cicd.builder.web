namespace Base2art.CiCd.Builder.Web
{
    using System.IO;

    public interface IDirectoryProvider
    {
        DirectoryInfo GetWorkingDir();
        DirectoryInfo DataDirectory();
    }
}