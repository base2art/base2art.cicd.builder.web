param ([string]$toolsPath)

if ([System.String]::IsNullOrWhitespace($toolsPath))
{
  $base = Resolve-Path "~"
  $toolsPath = "$base/.nuget/packages/base2art.webapirunner.server.runners.commandlineinterface/0.1.2.3/tools"
}

. "$($toolsPath)/Deployment.ps1"

$deploymentProcessor = New-Webapi-Deployment

$deploymentProcessor.bin('src/Base2art.CiCd.Builder.Web/bin/Release/netstandard2.0/')

# PAckaaging Example
$deploymentProcessor.package("Base2art.Standard.DataStorage.Provider.SQLite", "1.0.0.1")
$deploymentProcessor.package("Base2art.Web.App.Principals.Jwt", "1.3.0")

# config example
#$deploymentProcessor.config("input-file.yaml", "environment", "destinationName.yaml")
$deploymentProcessor.config("config/conf.yaml", "Production", "configuration.yaml")
$deploymentProcessor.config("config/urls.yaml", "Production", "urls.yaml")

Write-Host $deploymentProcessor.deploy()


Exit 0

