namespace Base2art.CiCd.Builder.Web.Features
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Security.Claims;
    using DataStorage;
    using DataStorage.InMemory;
    using DataStorage.Interception.DataDefinition;
    using DataStorage.Interception.DataManipulation;
    using FeatureDiscovery;
    using WebClient.ResponseHandlers.Json;
    using Data.dbo;
    using FluentAssertions;
    using Public.Endpoints;
    using Resources;
    using Xunit;

    public class UnitTest1
    {
        private class StoreFactory : IDataStoreFactory
        {
            private readonly DataStore store;

            public StoreFactory(DataStore store) => this.store = store;

            public IDataStore Create(string name) => this.store;
        }

        [Fact(Skip = "Ignore -SjY")]
        public async void RealWorldTest()
        {
            var definer = new DataDefiner(true);
            IDataDefinerAndManipulator wrapper = new WrappingDataStorage(new IDataDefinerInterceptor[] { },
                                                                         new IDataManipulatorInterceptor[] { },
                                                                         definer,
                                                                         definer);

            var store = new DataStore(wrapper);
            var dbms = new Dbms(wrapper);

            await dbms.CreateTable<node_build_v2>().Execute();
            await dbms.CreateTable<node_build_source_v2>().Execute();

            var scId = Guid.NewGuid();
            var builderService = new BuildService(
                                                  new StoreFactory(store),
//                                                  Guid.NewGuid(),
                                                  new DefinableDirectoryProvider("/builds-manual/unit-test-temp/"),
                                                  new RequireAuthProvider(false), 
                                                  true,
                                                  new IFeatureDiscoveryProvider[0]);

            var buildContent = new List<ProjectBuildContent>();
            buildContent.Add(new ProjectBuildContent
                             {
                                 SourceControlId = scId,
                                 Files = File.ReadAllBytes(@"Z:\b2a\cicd\tests\Base2art.CiCd.Builder.Web.Features\base2art.standard.webapirunner.zip"),
                                 BranchHash = Guid.NewGuid().ToString("N"),
                                 BranchName = "master",
                                 BuildInstructions = new[]
                                                     {
                                                         new BuildInstruction
                                                         {
                                                             Executable = "pwsh",
                                                             Arguments = new[] {"-File", "bob.ps1", "default"}
                                                         }
                                                     }
                             });

            var buildId = Guid.NewGuid();
            var pb = await builderService.StartBuild(buildId, "", buildContent, ClaimsPrincipal.Current);

            while (!(pb.State == BuildPhaseState.CompletedSuccess || pb.State == BuildPhaseState.CompletedFail))
            {
                pb = await builderService.GetBuild(buildId, ClaimsPrincipal.Current);
            }

            var artifacts = await builderService.GetArtifacts(buildId, ClaimsPrincipal.Current);

            artifacts[scId].Length.Should().BeGreaterThan(5);
            await builderService.Remove(buildId, ClaimsPrincipal.Current);

            var artifacts1 = await builderService.GetArtifacts(buildId, ClaimsPrincipal.Current);

            artifacts1[scId].Length.Should().BeGreaterThan(5);
        }

        [Fact]
        public async void Test1()
        {
            var data =
                @"{""buildId"":""9547a811-2b04-41c4-8de7-8f5228b58554"",""state"":""Pending"",""completedAt"":null,""output"":"""",""error"":""""}";
            ProjectBuild pb = null;
            var handler = new JsonHandler<ProjectBuild>(build => pb = build);

            await handler.Invoke(data);

            pb.State.Should().Be(BuildPhaseState.Pending);
        }
    }
}