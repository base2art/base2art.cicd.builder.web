param ([Parameter(Mandatory = $false)][string]$fullPath)

$basePath = Resolve-Path "~"
$pwd = split-path -parent $MyInvocation.MyCommand.Definition

$data = @{
    profiles = @{
        "Default-VisualStudio" = @{
            commandName = "Executable"
            executablePath = "dotnet"
            commandLineArgs = "$basePath/.nuget/packages/base2art.webapirunner.server.runners.commandlineinterface/0.1.2.3/lib/netcoreapp2.2/Base2art.WebApiRunner.Server.Runners.CommandLineInterface.dll server config/conf.yaml"
            workingDirectory = "$pwd"
        };
    }
}

$content = ConvertTo-Json $data

if ($fullPath -eq $null -or $fullPath -eq "")
{
    echo "FullPath: None"
    $srcs = Get-ChildItem -Path "src"
    foreach ($src in $srcs)
    {
        if (-not(Test-Path "$( $src.FullName )/Properties"))
        {
            mkdir "$( $src.FullName )/Properties"
        }
Set-Content -Path $debugPath -Value $content

        Set-Content -Path "$( $src.FullName )/Properties/launchSettings.json" -Value $content
    }

    exit 0;
}

$launchDir = "$( $fullPath )/Properties"
$launchPath = "$( $launchDir )/launchSettings.json"
if (Test-Path $launchPath)
{
    exit 0;
}


if (-not(Test-Path $launchDir))
{
    mkdir $launchDir
}

Set-Content -Path $launchPath -Value $content




